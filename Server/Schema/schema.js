import graphql , {GraphQLObjectType, GraphQLList, GraphQLID, GraphQLString, GraphQLInt, GraphQLSchema} from  'graphql'
import _ from 'lodash'
import Book from '../model/book'
import Author from '../model/author'

const BookType = new GraphQLObjectType({
    name: "book",
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        price: {type: GraphQLInt},
        author: {
            type: AuthorType,
            resolve(parent, args){
                return Author.find({where : {id: parent.authorid}})
            }
        }
    })
})

const AuthorType = new GraphQLObjectType({
    name: "author",
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        age: {type: GraphQLInt},
        book: {
            type: GraphQLList(BookType),
            resolve(parent, args){
                return Book.findAll({where: {authorid: parent.id}})
            }
        }
    })
})


const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        book: {
            type: BookType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                //code to get data from db
               return Book.find({where: {id: args.id}})
            }
        },
        author : {
            type: AuthorType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return Author.find({where: {id: args.id}})
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args){
                return Book.findAll()
            }
        },
        authors: {
            type: new GraphQLList(AuthorType),
            resolve(parent, args){
                return Author.findAll()
            }
        }
    }
})

const Mutation = new GraphQLObjectType({
    name: 'mutation',
    fields:{
        addBook: {
            type: BookType,
            args: {
                name: {type: GraphQLString},
                price: {type: GraphQLInt},
                authorid: {type: GraphQLInt}
            },
            resolve(parent, args){
                return Book.create({
                    name: args.name,
                    price: args.price,
                    authorid: args.authorid
                })
            }
        },
        updateBook: {
            type: BookType,
            args:{
                id: {type: GraphQLID},
                name: {type: GraphQLString},
                price: {type: GraphQLInt},
                authorid: {type: GraphQLInt}
            },
            resolve(parent, args) {
                return Book.update(
                    {name: args.name, price: args.price, authorid: args.authorid},
                    {where : {id: args.id}}
                    
                )
            }
        },
        deleteBook: {
            type: BookType,
            args: {
                id: {type: GraphQLID}
            },
            resolve(parent, args){
                return Book.destroy({
                    where: {id: args.id}
                })
            }
        },
        addAuthor: {
            type: AuthorType,
            args: {
                name: {type: GraphQLString},
                age: {type: GraphQLInt}
            },
            resolve(parent, args){
                return Author.create({
                    name: args.name,
                    age: args.age
                })
            }
        },
        updateAuthor: {
            type: AuthorType,
            args:{
                id: {type: GraphQLID},
                name: {type: GraphQLString},
                age: {type: GraphQLInt}
            },
            resolve(parent, args) {
                 Author.update(
                    {name: args.name, age: args.age},
                    {where : {id: args.id}}
                    
                )
                .then(result => {result})
                .catch(err => {err})
            }
        },
        deleteAuthor: {
            type: AuthorType,
            args: {
                id: {type: GraphQLID}
            },
            resolve(parent, args){
                return Author.destroy({
                    where: {id: args.id}
                })
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})