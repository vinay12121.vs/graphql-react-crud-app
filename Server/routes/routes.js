import express from 'express'
import graphqlHTTP from 'express-graphql'
import schema from '../Schema/schema'
// import bookRoutes from './book-routes'
let router = express.Router();
router.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))
// router.use('/book', bookRoutes)


router.use((req, res, next) => {
    console.log('Welcome To Sequelize Demo!!')
    res.send("Welcome To Express Demo!!")
})

module.exports = router
