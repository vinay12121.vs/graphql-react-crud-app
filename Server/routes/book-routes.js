import express from 'express'
import bookController from '../controller/book-controller'
let Router = express.Router()


Router.post('/add-book', bookController.addBook)
Router.post('/delete-book', bookController.removeBook)
Router.post('/update-book', bookController.updateBook)
Router.get('/get-book', bookController.getOneBook)
// Router.get('/getall-book')

module.exports =router