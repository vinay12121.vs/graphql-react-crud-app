import express from 'express'
import env from 'dotenv'
import db from './config/config'
import router from './routes/routes'
import cors from 'cors'
let app = express()
env.load()
app.use(cors())
app.use(router)

let port = process.env.PORT || 1234
app.listen(port, () => console.log(`Listening at ${port}`))