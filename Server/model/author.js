import Sequelize from 'sequelize'
import db from '../config/config'
let authorModel = db.define('author', {
    name: Sequelize.STRING,
    age: Sequelize.INTEGER
}, {
    timestamps: false
})

module.exports = authorModel
