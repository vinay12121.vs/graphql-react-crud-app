import Sequelize from 'sequelize'
import db from '../config/config'
let bookModel = db.define('book', {
    name: Sequelize.STRING,
    price: Sequelize.INTEGER,
    authorid: Sequelize.INTEGER
},
{
    timestamps: false
})

module.exports = bookModel