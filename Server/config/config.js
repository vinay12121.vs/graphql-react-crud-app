import Sequelize from 'sequelize'
import env from 'dotenv'
env.load()
const connection = new Sequelize(process.env.DB, process.env.ROLE, process.env.PASSWORD, {
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    operatorsAliases: false,
  
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  
  });
  
connection.authenticate()
.then(() => console.log("Connected to sequelize Successfully!!"))
.catch(err => console.log(`Error While Connecting to sequelize \n ${err}`))

module.exports = connection