import React, { Component } from 'react';
import {graphql} from 'react-apollo'
import {getBookQuery} from '../queries/queries'

class BookDetails extends Component {
    displayBookDetails() {
        console.log(this.props)
    }
    render(){
        // console.log(this.props)
        return(
            <div className="book-details">
                {this.displayBookDetails()}
            </div>
        )
    }
}

export default graphql(getBookQuery)(BookDetails);
