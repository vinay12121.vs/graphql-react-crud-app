import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import {graphql, compose} from 'react-apollo'
import {getAuthorsQuery, addBookQuery, getBooksQuery} from '../queries/queries'


class AddBook extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            price: 0,
            authorid:''
        }
    }
    displayAuthors(){
        // console.log(this.props)
        let data = this.props.getAuthorsQuery
        if(data.loading)
            return(<option>Loading Authors...</option>)
        else
            return data.authors.map(author => {
                return (<option key={author.id} value={author.id}>{author.name}</option>)
            })
    }

    submitHandler(e) {
        e.preventDefault()
        // console.log(this.props.addBookQuery)
        this.props.addBookQuery(
            {
                variables:{
                    name: this.state.name,
                    price: parseInt(this.state.price),
                    authorid: parseInt(this.state.authorid)
                },
                refetchQueries: [{query: getBooksQuery}]
            }
        )
        
    }


  render() {      
    return(
        <form id="add-book" onSubmit={this.submitHandler.bind(this)}>

            <div className="field" onChange={(e) => this.setState({name: e.target.value})}>
                <label>Book Name:</label>
                <input type="text"/>        
            </div>

            <div className="field" onChange={(e) => this.setState({price: e.target.value})}>
                <label>Price:</label>
                <input type="text"/>        
            </div>
            <div className="field">
                <label>Author:</label>
                <select onChange={(e) => this.setState({authorid: e.target.value})}>
                    <option>Select Author</option>
                    {this.displayAuthors()}
                </select>                       
            </div>
            <button>+</button> 
        </form>
    )
  }
}

export default compose(
    graphql(getAuthorsQuery, {name:"getAuthorsQuery"}),
    graphql(addBookQuery, {name: "addBookQuery"})
)(AddBook);
