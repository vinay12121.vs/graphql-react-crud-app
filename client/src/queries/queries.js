import {gql} from 'apollo-boost'

const getAuthorsQuery = gql`
    {
        authors{
            name
            age
            id
        }
    }
`

const getBooksQuery = gql`
    {
        books {
            name
            id
        }
    }
`

const addBookQuery = gql`
mutation($name: String!, $price: Int!, $authorid: Int!) {
    addBook(name:$name, price:$price, authorid:$authorid){
        name
        price
    }
}
`

const getBookQuery = gql`
query($id:ID){
    book(id: $id){
        id
        name
        price
        author{
            id
            name
            age
            book{
                name
                id
            }
        }
    }
}
   
`
export {getBookQuery, getAuthorsQuery, getBooksQuery, addBookQuery}
